---
title: "CALIDAD DEL AIRE EN ANTIOQUIA: PM10 (2011-2018)"
output: 
  flexdashboard::flex_dashboard:
    logo: logo.png
    favicon: logo.png
    orientation: columns
    vertical_layout: fill
    source_code: embed
    theme:
      bootswatch: "minty"
      base_font:
        google: Raleway
      heading_font:
        google: Sedgwick Ave
      code_font: VT323
---

```{r setup, include=FALSE}
library(flexdashboard)
```


```{r eval=FALSE}
#https://www.datos.gov.co/Ambiente-y-Desarrollo-Sostenible/DATOS-DE-CALIDAD-DEL-AIRE-EN-COLOMBIA-2011-2018/ysq6-ri4e

delim_file <- "DATOS_DE_CALIDAD_DEL_AIRE_EN_COLOMBIA_2011-2018.csv"
readLines(delim_file, n = 2)

library(tidyverse)
library(janitor)

read_csv("DATOS_DE_CALIDAD_DEL_AIRE_EN_COLOMBIA_2011-2018.csv", 
         col_types = cols('Autoridad Ambiental' = col_skip(), 
                          'Código del departamento' = col_skip(), 
                          'Código del municipio' = col_skip(), 
                          'Nueva columna georreferenciada' = col_skip())) %>% 
  clean_names() %>% filter(departamento == "ANTIOQUIA") %>% 
  write_rds(compress = "xz", file = "calidad_aire_depurada.rds")
```


Column {data-width=550}
-----------------------------------------------------------------------

### Estaciones de medición 

```{r}
library(tidyverse)
library(lubridate)

calidad_aire <- read_rds("calidad_aire_depurada.rds") %>% 
  filter(variable == "PM10") %>% #No es la variable definitiva
  separate(fecha, c("fecha", "hora"), sep = " ") %>% 
  mutate(across(c(tecnologia, 
                  departamento:tipo_de_estacion, 
                  variable, 
                  unidades), as.factor), 
         fecha = as_date(fecha, format = "%d/%m/%Y"), 
         year = year(fecha), 
         mes = month(fecha, label = TRUE), 
         dia = yday(fecha)) %>% 
  na.omit() %>% 
  select(-c(hora, unidades, tiempo_de_exposicion, variable, departamento))
```

```{r}
library(osmdata)
library(ggmap)
library(plotly)
library(RColorBrewer)

ca_mapa <- calidad_aire %>% 
  mutate() %>% 
  group_by(year, mes, latitud, longitud, nombre_de_la_estacion) %>% 
  summarise(prom_con = mean(concentracion)) %>% 
  mutate(alerta = if_else(prom_con < 40, "Sano", 
                          if_else(prom_con >= 40 & prom_con <= 100, "Precaución", 
                                  if_else(prom_con > 100, "Peligro", "NA"))), 
         year = as_factor(year))#,
         #alerta = fct_relevel(alerta, "Sano"))

colores <- c("red", "yellow", "green")
names(colores) <- levels(ca_mapa$alerta)


ggplotly(
  ggmap(get_map(getbb("Antioquia"), 
              maptype = "toner-background")) + 
    geom_point(ca_mapa, 
               shape = 22, 
               mapping = aes(x = longitud, y = latitud, 
                             fill = alerta, 
                             text = prom_con, 
                             label = nombre_de_la_estacion, 
                             color = year), 
               size = 3.5, 
               alpha = 0.5) + 
    scale_fill_manual(values = colores) + 
    labs(x = "Longitud", y = "Latitud") + 
    theme_minimal()
)
```

> En verde, concentraciones menores a 40 μg/m³ de PM10. En amarillo, menores a 100 μg/m³ de PM10. En rojo, mayores a 100 μg/m³ de PM10. 



Column {data-width=450}
-----------------------------------------------------------------------

### Concentración 2011-2018

```{r warning=FALSE}
library(gganimate)

anim <- calidad_aire %>%  
  group_by(fecha, year, mes, dia) %>% 
  summarise(con = mean(concentracion)) %>%
  ggplot(aes(x = fecha, y = con)) + 
  geom_line() + 
  labs(x = "Mes", y = "Concentración PM10 (μg/m³)") + 
  facet_wrap(~year, scales = "free_x") + 
  scale_x_date(date_labels = "%b", 
               date_breaks = "2 month") +
  transition_reveal(dia)

animate(anim)
```

### Base de datos

```{r}
library(DT)

calidad_aire %>% 
  mutate(mes = str_to_sentence(mes), 
         mes = fct_relevel(mes, "Ene", "Feb", "Mar", "Abr", "May", 
                           "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic")) %>% 
  group_by(year, mes, nombre_del_municipio, nombre_de_la_estacion, tipo_de_estacion) %>% 
  summarise(media_pm10 = round(mean(concentracion), 1), 
            mediana_pm10 = round(median(concentracion), 1), 
            desviacion_pm10 = round(sd(concentracion), 1), 
            q5_pm10 = round(quantile(concentracion, .05), 1), 
            q98_pm10 = round(quantile(concentracion, .98), 1)) %>% 
  datatable(rownames = FALSE)
```

